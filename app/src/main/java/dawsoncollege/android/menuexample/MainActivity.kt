package dawsoncollege.android.menuexample

import android.content.Intent
import android.os.Bundle
import android.provider.ContactsContract
import android.view.ContextMenu
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView.AdapterContextMenuInfo
import android.widget.ArrayAdapter
import android.widget.PopupMenu
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import dawsoncollege.android.menuexample.databinding.ActivityMainBinding

// see https://developer.android.com/develop/ui/views/components/menus
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private lateinit var datasource: MutableList<String>
    private lateinit var adapter: ArrayAdapter<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        /*
         * Remember the 3 things we need for an AdapterView :
         * 1. A datasource (in our case, it's just a list we get from resources)
         * 2. An adapter (in our case, it's just an ArrayAdapter, with a default layout for each
         * of its item)
         * 3. An adapter view (in our case, it's the ListView)
         */
        datasource = resources.getStringArray(R.array.contacts).toMutableList()
        adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, datasource)

        binding.contactsListview.adapter = adapter

        /*
         * We call `RegisterForContextMenu()` on every View we want a ContextMenu attached. For an
         * AdapterView, it's a bit different : we may call `registerForContextMenu()` we call on
         * the AdapterView itself, but the ContextMenu will be attached on every contained item.
         */
        registerForContextMenu(binding.contactsListview)

        /*
         * This onClickListener is set for every item in the ListView, it's what displays the
         * PopupMenu related to the clicked item
         */
        binding.contactsListview.setOnItemClickListener { _, view, _, _ -> showPopupMenu(view) }
    }

    //
    // Functions for OptionsMenu...
    //

    /**
     * Called when the OptionsMenu is needed (which is at the start of the Activity's lifecycle,
     * because the OptionsMenu is always visible at the top)
     */
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)

        menuInflater.inflate(R.menu.options_menu, menu)
        return true
    }

    /**
     * Handles the user click on the OptionsMenu. The MenuItem that was clicked is passed as a
     * parameter.
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.sort_alpha_menu_item -> {
                datasource.sort()
                adapter.notifyDataSetChanged()
                true
            }
            R.id.shuffle_menu_item -> {
                datasource.shuffle()
                adapter.notifyDataSetChanged()
                true
            }
            R.id.info_menu_item -> {
                val count = datasource.count()
                val message = resources.getQuantityString(R.plurals.you_have, count, count)
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    //
    // Functions for PopupMenu...
    //

    /**
     * Displays the PopupMenu. It is anchored to the View passed as a parameter
     */
    private fun showPopupMenu(v: View) {
        PopupMenu(this, v).apply {
            setOnMenuItemClickListener { item ->
                onPopupMenuItemClick(v, item)
            }
            inflate(R.menu.popup_menu)
            show()
        }
    }

    /**
     * Handles the user click on the PopupMenu.
     *
     * @param v The View on which the PopupMenu is anchored
     * @param item The MenuItem that was selected
     *
     * @return True if and only if, the click event was handled. False, otherwise
     */
    private fun onPopupMenuItemClick(v: View, item: MenuItem): Boolean {
        val clickedString = (v as TextView).text.toString()

        return when (item.itemId) {
            R.id.show_email_menu_item -> {
                val email = createEmailFromName(clickedString)
                val message = resources.getString(R.string.email_is, email)
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
                true
            }
            R.id.create_contact_menu_item -> {
                launchCreateContactIntent(clickedString)
                true
            }
            else -> false
        }
    }

    private fun createEmailFromName(name: String): String =
        name.lowercase().replace(" ", ".") + "@gmail.com"

    private fun launchCreateContactIntent(contactName: String) {
        val contactEmail = createEmailFromName(contactName)

        // see https://developer.android.com/training/contacts-provider/modify-data#InsertContact
        val intent = Intent(ContactsContract.Intents.Insert.ACTION).apply {
            type = ContactsContract.RawContacts.CONTENT_TYPE
            putExtra(ContactsContract.Intents.Insert.NAME, contactName)

            putExtra(ContactsContract.Intents.Insert.EMAIL, contactEmail)
            putExtra(
                ContactsContract.Intents.Insert.EMAIL_TYPE,
                ContactsContract.CommonDataKinds.Email.TYPE_WORK
            )
        }

        startActivity(intent)
    }

    //
    // Functions for ContextMenu...
    //

    /**
     * Creates the ContextMenu for a given item in the ListView, just before it's shown to the user.
     */
    override fun onCreateContextMenu(
        menu: ContextMenu?, v: View?, menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        super.onCreateContextMenu(menu, v, menuInfo)

        menuInflater.inflate(R.menu.context_menu, menu)

        // Giving the ContextMenu a title (the name of the contact)
        if (menuInfo != null) {
            val position = (menuInfo as AdapterContextMenuInfo).position

            menu?.setHeaderTitle(adapter.getItem(position))

            // Only allow an item to be moved up/first, if it isn't first
            menu?.setGroupEnabled(R.id.move_up_menu_group, !adapter.isFirst(position))
            // Only allow an item to be moved down/last, if it isn't last
            menu?.setGroupEnabled(R.id.move_down_menu_group, !adapter.isLast(position))
        }
    }

    /**
     * Handles the user click on the ContextMenu. The MenuItem that was clicked is passed as a
     * parameter.
     */
    override fun onContextItemSelected(item: MenuItem): Boolean {
        val clickedPosition = (item.menuInfo as AdapterContextMenuInfo).position
        val clickedString = adapter.getItem(clickedPosition) ?: return false

        // action depends on the MenuItem that was selected
        return when (item.itemId) {
            R.id.delete_menu_item -> {
                adapter.remove(clickedString)
                true
            }
            R.id.move_first_menu_item -> {
                datasource.moveToFirst(clickedPosition)
                adapter.notifyDataSetChanged()
                true
            }
            R.id.move_up_menu_item -> {
                datasource.moveOneBefore(clickedPosition)
                adapter.notifyDataSetChanged()
                true
            }
            R.id.move_down_menu_item -> {
                datasource.moveOneAfter(clickedPosition)
                adapter.notifyDataSetChanged()
                true
            }
            R.id.move_last_menu_item -> {
                datasource.moveToLast(clickedPosition)
                adapter.notifyDataSetChanged()
                true
            }
            else -> super.onContextItemSelected(item)
        }
    }
}

fun <T> ArrayAdapter<T>.isFirst(position: Int): Boolean = count > 0 && position == 0
fun <T> ArrayAdapter<T>.isLast(position: Int): Boolean = count - 1 == position

fun <T> MutableList<T>.moveToFirst(position: Int) = add(0, removeAt(position))

fun <T> MutableList<T>.moveOneBefore(position: Int) {
    val temp = this[position - 1]
    this[position - 1] = this[position]
    this[position] = temp
}

fun <T> MutableList<T>.moveOneAfter(position: Int) {
    val temp = this[position + 1]
    this[position + 1] = this[position]
    this[position] = temp
}

fun <T> MutableList<T>.moveToLast(position: Int) = add(removeAt(position))